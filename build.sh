#!/bin/bash

# The sheepit client images use staged builds to reduce the amount of Dockerfiles and minimize duplicate build instructions
# The BASEIMAGE build argument parameter is used to tell the stages of Dockerfile.shared-stage and Dockerfile.nvidia-legacy which image to continue from

docker build --pull --build-arg FULL_VERSION=0.1.dev -f ./build/Dockerfile.text-base -t sheepit-client:text-base ./build/
docker build --build-arg FULL_VERSION=0.1.dev --build-arg BASEIMAGE=sheepit-client:text-base -f ./build/Dockerfile.shared-stage -t sheepit-client:text ./build/

docker build --pull --build-arg FULL_VERSION=0.1.dev -f ./build/Dockerfile.gui-base -t sheepit-client:gui-base ./build/
docker build --build-arg FULL_VERSION=0.1.dev --build-arg BASEIMAGE=sheepit-client:gui-base -f ./build/Dockerfile.shared-stage -t sheepit-client:gui ./build/

# Similarly, the docker container used for test-cases is handled that way, using the text tag as a base
docker build --build-arg FULL_VERSION=0.1.dev --build-arg BASEIMAGE=sheepit-client:text -f ./test/Dockerfile.test -t sheepit-client:text-test ./test/
echo "You may now run \"docker run --rm sheepit-client:text-test\" to run the test cases"