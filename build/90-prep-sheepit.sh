#!/usr/bin/with-contenv sh

#
# Take ownership of files and directories under /config.
#

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

log() {
    echo "[cont-init.d] $(basename $0): $*"
}
if ! chown -R $USER_ID:$GROUP_ID /sheepit; then
    log "Check2"
    # Transferring dir and file ownerships
    # Makes sure the renderer can even operate

    # Failed to transfer ownership of /sheepit. This could happen when,
    # for example, the folder is mapped to a network share.
    log "ERROR: Failed to transfer ownership of /sheepit."
    exit 1
fi
if [ "$SHEEP_THEME" = "dark" ]; then
    if ! sed -i -e 's/<body>/<body style="background-color: #292929">/g' /opt/novnc/index.html; then
        # Make background actually dark
        # A better way to accomplish a dark background hasn't been found yet

        # This could happen if the file changes upstream
        log "ERROR: Failed to set background to dark."
        exit 1
    fi
fi

# vim:ft=sh:ts=4:sw=4:et:sts=4