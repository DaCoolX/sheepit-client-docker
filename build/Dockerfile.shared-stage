# Pull base image supplied via build-arg
ARG BASEIMAGE=unknown
FROM ${BASEIMAGE}

ARG FULL_VERSION=dev
ENV VERSION=$FULL_VERSION

LABEL maintainer="Laurent Clouet <laurent.clouet@nopnop.net>"
LABEL version=$VERSION

ARG DEBIAN_FRONTEND=noninteractive
# Makes the apt-get process aware
# of the docker build process env
# so it doesn't complain as much

# For Nvidia cuda support
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility

RUN \
	mkdir -p \
        /sheepit/client \
	    /sheepit/cache \
        /sheepit/shared-zip && \
	# Upgrade packages, install curl and blender dependencies
	test -f /etc/apt/sources.list && sed -i '/^\([^#].*main\)*$/s/main/& contrib non-free/' /etc/apt/sources.list || true && \
    test -f /etc/apt/sources.list.d/debian.sources && sed -i '/^\([^#].*main\)*$/s/main/& contrib non-free/' /etc/apt/sources.list.d/debian.sources || true && \
	apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y --no-install-recommends  \
	    curl \
	    libgl1 \
	    libglu1-mesa \
	    libsm6 \
	    libxfixes3 \
	    libxi6 \
	    libxkbcommon0 \
	    libxrender1 \
	    libnvoptix1 \
	    libxxf86vm1 && \
	# (potential) Blender dependencies explained:
	# needed in the past:
	#	libsdl1.2debian
	# openjdk-11-jre-headless dependencies:
	#	libfreetype6
	apt-get -y autoremove && \
	apt-get -y clean && \
	rm -rf \
		/var/lib/apt/lists/* \
		/tmp/*

# Copying scripts here to keep /sheepit/client consistent across all tags.
ADD startapp.sh /sheepit/client/startapp.sh
ADD initapp.sh /sheepit/client/initapp.sh
# Init script will drop root for start script

WORKDIR /sheepit/client
VOLUME /sheepit/client
VOLUME /sheepit/cache
VOLUME /sheepit/shared-zip

ENV USER_ID "1000"
ENV GROUP_ID "1000"
ENV SHEEP_CACHE_DIR "/sheepit/cache"
ENV SHEEP_SHARED_ZIP "/sheepit/shared-zip"
ENV SHEEP_HOSTNAME "SheepIt-Docker"
ENV SHEEP_HEADLESS "true"