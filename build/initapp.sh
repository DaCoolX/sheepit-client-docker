#!/bin/bash
set -euo pipefail # Unofficial strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/

cd "$(dirname "$(readlink -f "$0")")"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

set +e # Allow them to fail
userdel sheepit 2>/dev/nul # They may already exist
groupdel sheepit 2>/dev/nul
set -e

groupadd --gid "$GROUP_ID" sheepit
useradd --no-create-home \
        --home-dir /sheepit/client/ \
        --uid "$USER_ID" \
        --gid "$GROUP_ID" \
        --inactive 0 \
        sheepit
# Adding a sheepit group and user

chown -R sheepit:sheepit /sheepit/
# Setting file and dir permissions and ownerships
# Makes sure the renderer can even operate

su --command "/sheepit/client/startapp.sh" \
        --preserve-environment \
        sheepit
# Running sheepit as the configured user with the environment preserved because the startapp script relies on it
