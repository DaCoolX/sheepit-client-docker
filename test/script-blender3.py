import bpy
import bpy, sys
bpy.context.scene.cycles.use_auto_tile = True
bpy.context.scene.cycles.tile_size = 2048
def sheepit_set_compute_device(device_type, device_family, device_id):
	# either OPTIX, GPU, OPTIX_0
	# or     NONE,  CPU, CPU

	bpy.context.scene.cycles.device = device_family
	
	# fill up the devices list
	bpy.context.preferences.addons['cycles'].preferences.get_devices()
	
	bpy.context.preferences.addons['cycles'].preferences.compute_device_type = device_type
	devices = bpy.context.preferences.addons['cycles'].preferences.devices
	compatible_devices = [d for d in devices if (d.type == device_type or (d.type == 'CPU' and device_type == 'NONE')) and d.id == device_id]
	if not compatible_devices:
		sys.exit(f"DETECT_DEVICE_ERROR: Couldn't find {device_type} device with id {device_id}")

	for device in devices:
		device.use = device in compatible_devices
bpy.context.scene.render.image_settings.file_format = 'PNG'
bpy.context.scene.render.use_stamp = False
## use border and crop
bpy.context.scene.render.use_border = True
bpy.context.scene.render.use_crop_to_border = True
## set border for this tile
bpy.context.scene.render.border_min_x = 0
bpy.context.scene.render.border_max_x = 1
bpy.context.scene.render.border_min_y = 0
bpy.context.scene.render.border_max_y = 1
import bpy

# bpy.context.scene.render.threads_mode = 'AUTO'

# if it's a movie clip, switch to png
fileformat = bpy.context.scene.render.image_settings.file_format
if fileformat != 'BMP' and fileformat != 'PNG' and fileformat != 'JPEG' and fileformat != 'TARGA' and fileformat != 'TARGA_RAW':
	bpy.context.scene.render.image_settings.file_format = 'PNG'
	bpy.context.scene.render.filepath = ''

if bpy.context.scene.render.image_settings.file_format == 'PNG':
	bpy.context.scene.render.image_settings.compression = 100

try:
	bpy.context.scene.render.use_stamp_filename = False
	bpy.context.scene.render.use_stamp_date = False
	bpy.context.scene.render.use_stamp_hostname = False
	bpy.context.scene.render.use_stamp_memory = False
	bpy.context.scene.render.use_stamp_render_time = False
except AttributeError:
	pass

try:
	bpy.context.scene.render.threads_mode = 'AUTO'
except AttributeError:
	pass

try:
	bpy.context.scene.render.use_persistent_data = False
except AttributeError:
	pass


sheepit_set_compute_device("NONE", "CPU", "CPU")
import signal
def hndl(signum, frame):
    pass
signal.signal(signal.SIGINT, hndl)

