# SheepIt Client in Docker

Official docker image for the SheepIt Renderfarm client

## Text UI Quick-start

As a quick start you might use the text ui example below where you substitute `<login>` and `<password>` for your SheepIt username and password (or preferably renderkey) respectively.
You can attach your terminal to the text ui of the container with `docker attach sheepit` also detach your terminal again (Default escape sequence is `Ctrl+P + Ctrl+Q`).

```bash
docker run -dit \
 --name sheepit \
 -e SHEEP_LOGIN=<login> \
 -e SHEEP_PASSWORD=<password> \
 sheepitrenderfarm/client:text
```

## Web GUI Quick-start

As a quick start you might use the web gui example below where you navigate to `http://<host-ip-addr>:5800` in order to access the graphical frontend similar to a Desktop application.  
`<host-ip-addr>` being the IP address/hostname/FQDN of the docker host you started the container on, i.e for your local pc at `http://localhost:5800`.

```bash
docker run -d \
 --name sheepit \
 -p 5800:5800 \
 sheepitrenderfarm/client:gui
```

## Tags

### Text

Text UI (Commandline) images are available under the tag `text`.  
The `text` tag is also the default tag: `latest`.

### Graphical-User-Interface over HTTP

Web GUI (Desktop) images are available under the tag `gui`.

### Version-specific Tags

There are also version-specific tags available:  
for example for the tag `text` and a version `1.2.3`  
these would be `text-1.2` and `text-1.2.3`.

Please consult for [GitLab Tags](https://gitlab.com/sheepitrenderfarm/client-docker/-/tags)
or [Docker Hub Tags](https://hub.docker.com/r/sheepitrenderfarm/client/tags) for more information.

## Configuration

### User and Group IDs

If you want to change the UID and GID of the running user you can use the `USER_ID` and `GROUP_ID` environment variables respectively.  
They both default to `1000`.

### Beta

You can use the beta branch for the client via setting an environment variable named `BETA` to `true`

### SheepIt parameters

All SheepIt client parameters can be controlled via environment variables.  
For an explanation of the parameters, consider running `docker run --rm -e SHEEP_HELP=true sheepitrenderfarm/client:text`.

#### All SheepIt variables:

| Command Line Argument | Environment Variable   | Notes                                          |
|-----------------------|------------------------|------------------------------------------------|
| `--help`              | `SHEEP_HELP`           | Set to `true` to activate                      |
| `--headless`          | `SHEEP_HEADLESS`       | Set to `true` to activate, default `true`      |
| `--no-gpu`            | `SHEEP_NO_GPU`         | Set to `true` to activate                      |
| `--no-systray`        | `SHEEP_NO_SYSTRAY`     | Set to `true` to activate                      |
| `--show-gpu`          | `SHEEP_SHOW_GPU`       | Set to `true` to activate                      |
| `--verbose`           | `SHEEP_VERBOSE`        | Set to `true` to activate                      |
| `--version`           | `SHEEP_VERSION`        | Set to `true` to activate                      |
| `-cache-dir`          | `SHEEP_CACHE_DIR`      | Defaults to volume `/sheepit/cache`            |
| `-compute-method`     | `SHEEP_COMPUTE_METHOD` |                                                |
| `-config`             | `SHEEP_CONFIG`         |                                                |
| `-cores`              | `SHEEP_CORES`          |                                                |
| `-extras`             | `SHEEP_EXTRA`          |                                                |
| `-gpu`                | `SHEEP_GPU`            |                                                |
| `-hostname`           | `SHEEP_HOSTNAME`       | Defaults to `SheepIt-Docker`                   |
| `-login`              | `SHEEP_LOGIN`          |                                                |
| `-memory`             | `SHEEP_MEMORY`         |                                                |
| `-password`           | `SHEEP_PASSWORD`       |                                                |
| `-priority`           | `SHEEP_PRIORITY`       |                                                |
| `-proxy`              | `SHEEP_PROXY`          |                                                |
| `-rendertime`         | `SHEEP_RENDERTIME`     |                                                |
| `-request-time`       | `SHEEP_REQUEST_TIME`   |                                                |
| `-server`             | `SHEEP_SERVER`         |                                                |
| `-shared-zip`         | `SHEEP_SHARED_ZIP`     | Defaults to volume `/sheepit/shared-zip`       |
| `-shutdown`           | `SHEEP_SHUTDOWN`       |                                                |
| `-shutdown-mode`      | `SHEEP_SHUTDOWN_MODE`  |                                                |
| `-theme`              | `SHEEP_THEME`          | Defaults to `dark` for Gui                     |
| `-title`              | `SHEEP_TITLE`          |                                                |
| `-ui`                 | `SHEEP_UI`             | Defaults to `text` or `swing` depending on tag |

### More advanced configuration options

### Docker compose

Take a look inside the `docker-compose.yaml` or `gui-docker-compose.yaml` sample file.

#### Nvidia support

In order to enable Nvidia GPU support, you need Docker >= 19.03 and the latest [NVIDIA driver](https://github.com/NVIDIA/nvidia-docker/wiki/Frequently-Asked-Questions#how-do-i-install-the-nvidia-driver) and `nvidia-docker2` installed on your host system.
An official guide by Nvidia can be found [here](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#installing-on-ubuntu-and-debian).
Don't forget to add `--gpus all` to you docker run options:
```bash
docker run -d \
 #[...]
 --gpus all \
 #[...]
```

#### Running multiple SheepIt containers

If one considers running multiple containers of SheepIt or the same hosts or clusters of hosts with a network-shared filesystem,
it would be beneficial and ecologic to share the `/sheepit/shared-zip` directory.
For example using `/opt/sheepit/shared-zip` as the host directory to bind-mount it via `-v /opt/sheepit/shared-zip:/sheepit/shared-zip`
An addiotional modifier from above could look like this.

```bash
docker run -d \
 #[...]
 -v /opt/sheepit/shared-zip:/sheepit/shared-zip \
 #[...]
```

Furthermore `DOWNLOAD_ONLY="true"` can be used to only download the latest sheepit-client jar file,
caching the latest-version information into the `/sheepit/client` directory and exit.
This can be used with `NO_AUTOUPDATE="true"` on further container invocations
to keep a specific SheepIt client version and only update manually possibly distributing it to multiple SheepIt containers.

For example, running:

```bash
docker run \
 --name sheepit \
 -e DOWNLOAD_ONLY="true" \
 sheepitrenderfarm/client:text && \
docker cp -a sheepit:/sheepit/client /opt/sheepit/client
```

would enable you to run multiple SheepIt containers with a shared `/sheep/client` and `/sheep/shared-zip` along the lines of:

```bash
docker run -d \
 #[...]
 -e NO_AUTOUPDATE="true" \
 -v /opt/sheepit/client:/sheepit/client \
 -v /opt/sheepit/shared-zip:/sheepit/shared-zip \
 #[...]
```

#### Running Verbose mode long-term

Running with `SHEEP_VERBOSE` permanently set is only recommended while limiting the file-size of the log-file via `--log-opt max-size=10m`.

#### WebGui specific

As well as HTTP, offered by default on port `5800`, WebGui also offers VNC on port `5900`.  
Since the WebGui tag is based on this [docker-image](https://github.com/jlesage/docker-baseimage-gui/), all the configurations options thereof also apply to the WebGui.

## Building the images

Have a look inside `build.sh`.

## Links
[Docker Hub Repo page](https://hub.docker.com/r/sheepitrenderfarm/client)  

[SheepIt GitLab repo](https://gitlab.com/sheepitrenderfarm)   
[SheepIt site](https://www.sheepit-renderfarm.com/)

### Credit

dapor2000 @ [GitHub Repo](https://github.com/dapor2000/sheepit-docker)  
AGSPhoenix @ [GitHub Repo](https://github.com/AGSPhoenix/sheepit-docker)  
zocker-160 @ GitHub Repo [#1](https://github.com/zocker-160/sheepit-docker-webUI) and [#2](https://github.com/zocker-160/sheepit-docker)  
DaCool @ [GitHub Repo](https://github.com/DaCoolX/sheepit-docker)
